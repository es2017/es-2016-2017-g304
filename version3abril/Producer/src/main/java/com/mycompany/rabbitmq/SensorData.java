/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rabbitmq;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author aricson
 */
public class SensorData {
    
   public JSONObject GetData() {
        // TODO code application logic here
        JSONObject obj = null;
        try{                              
                obj = new JSONObject();
                obj.put("Temperatura", getRandomIntNumberInRange(0,100));
                obj.put("Humidade",getRandomIntNumberInRange(0,100));
                
                JSONArray list = new JSONArray();
                //Sensor RGB, Lumiosidade
                list.add(getRandomIntNumberInRange(0,255));
                list.add(getRandomIntNumberInRange(0,255));
                list.add(getRandomIntNumberInRange(0,255));
                
                obj.put("Lumiosidade", list);
                System.out.println(obj);
                //Espera durante 1s
                //Thread.sleep(1000);
        }catch(Exception ex){
            System.out.println("Erro: "+ex);
        }
        return obj;
    }
    
    private static int getRandomIntNumberInRange(int min, int max) {        
	if (min >= max) {
            throw new IllegalArgumentException("max tem que ser maior do que min");
	}
	return (int)(Math.random() * (max - min) + min);
}  
}
