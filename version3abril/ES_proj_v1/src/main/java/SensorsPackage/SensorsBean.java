package SensorsPackage;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@ManagedBean(name = "sensors")
@SessionScoped
public class SensorsBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private EventClient singleton;
    
    private static final ArrayList<Sensors> sensorsList = new ArrayList();;
    private Lumiosidade lumlist;
    private final JSONParser parser = new JSONParser();;
    
    public ArrayList<Sensors> getSensorsList() {
        return sensorsList;
    }

    private String getMensagem() {
        return singleton.getMensagem();
    }
    public void init(){
        if (!FacesContext.getCurrentInstance().isPostback()) {
            ProcessaJSON();
        }
    }
    
    public void ProcessaJSON(){
        try {
            
            JSONObject json = (JSONObject) parser.parse(getMensagem());
            long humidade = (Long) json.get("Humidade");
            long temperatura = (Long) json.get("Temperatura");
            JSONArray lumarray = (JSONArray) json.get("Lumiosidade");
            SetLumiosidade((Long)lumarray.get(0),(Long)lumarray.get(1),(Long)lumarray.get(2));
            SetSensor(lumlist,humidade,temperatura);
            
        } catch (ParseException ex) {
            Logger.getLogger(SensorsBean.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    private void SetSensor(Lumiosidade lum, long hum, long temp){
        sensorsList.add(new Sensors(lum, hum, temp));
    }
    
    private void SetLumiosidade(long r, long g, long b){
        lumlist = new Lumiosidade(r, g, b);
    }
    
    public static class Sensors {

        Lumiosidade luminosity;
        long humidity;
        long temperature;

        public Sensors(Lumiosidade luminosity, long humidity, long temperature) {

            this.luminosity = luminosity;
            this.humidity = humidity;
            this.temperature = temperature;

        }
    
        //getter and setter methods
        public Lumiosidade getLuminosity() {
            return luminosity;
        }

        public void setLuminosity(Lumiosidade luminosity) {
            this.luminosity = luminosity;
        }

        public long getHumidity() {
            return humidity;
        }

        public void setHumidity(long humidity) {
            this.humidity = humidity;
        }

        public long getTemperature() {
            return temperature;
        }

        public void setTemperature(long temperature) {
            this.temperature = temperature;
        }
    }
    
    public static class Lumiosidade{
        long r;
        long g;
        long b;

        public Lumiosidade(long r, long g, long b) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public long getR() {
            return r;
        }

        public void setR(long r) {
            this.r = r;
        }

        public long getG() {
            return g;
        }

        public void setG(long g) {
            this.g = g;
        }

        public long getB() {
            return b;
        }

        public void setB(long b) {
            this.b = b;
        }
        
    }

}
