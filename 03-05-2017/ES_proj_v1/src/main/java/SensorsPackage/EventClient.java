/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage;

/**
 *
 * @author aricson
 */
import java.net.URI;
import java.util.LinkedList;
import java.util.Queue;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import javax.websocket.ContainerProvider;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.eclipse.jetty.util.component.LifeCycle;

@Startup 
@Singleton
public class EventClient
{
    private Session session = null;
    private final URI uri = URI.create("ws://localhost:8180/events/");
    private Queue<String> queue = new LinkedList();
       
    @PostConstruct
    public void Load()
    {       

        try
        {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();

            try
            {
                // Attempt Connect
                session = container.connectToServer(EventSocket.class,uri);
                //while(true){
                     System.out.println("Connected to WS endpoint ");
                     
            session.addMessageHandler(new MessageHandler.Whole<String>() {

                @Override
                public void onMessage(String msg) {
                    queue.add(msg);
                    System.out.println(msg);
                        //SetMessage(msg);
                }
            });
            }
            finally
            {

                if (container instanceof LifeCycle)
                {
                    ((LifeCycle)container).stop();
                }
            }
        }
        catch (Throwable t)
        {
            t.printStackTrace(System.err);
        }
    }
    
    public Queue<String> getQueue() {
        return queue;
    }
    /*
    @PreDestroy
    public void destroy() {
        close();
    }

    private void close() {
        try {
            session.close();
            System.out.println("CLOSED Connection to WS endpoint ");
        } catch (IOException ex) {
            Logger.getLogger(EventClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/

}
