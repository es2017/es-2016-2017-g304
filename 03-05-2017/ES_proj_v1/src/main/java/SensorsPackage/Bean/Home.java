/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage.Bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author aricson
 */
public class Home implements Serializable{
    
    private long id_home;
    private String name;
    private ArrayList<SensorEquipament> sequip;

    public Home(long id_home, String name, ArrayList<SensorEquipament> sequip) {
        this.id_home = id_home;
        this.name = name;
        this.sequip = sequip;
    }

    public long getId_home() {
        return id_home;
    }

    public void setId_home(long id_home) {
        this.id_home = id_home;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SensorEquipament> getSequip() {
        return sequip;
    }

    public void setSequip(ArrayList<SensorEquipament> sequip) {
        this.sequip = sequip;
    }
    
    
}
