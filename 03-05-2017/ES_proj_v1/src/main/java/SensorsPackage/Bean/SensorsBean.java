package SensorsPackage.Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import SensorsPackage.EventClient;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.Queue;

@ManagedBean(name = "sensors")
@SessionScoped
public class SensorsBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private EventClient singleton;

    private final ArrayList<Home> sensorsList = new ArrayList();
    private Queue<String> bean_queue = new LinkedList();

    private final JSONParser parser = new JSONParser();

    public ArrayList<Home> getSensorsList() {
        return sensorsList;
    }

    private String getMensagem() {

        String str_queue = null;
        if (!(bean_queue = singleton.getQueue()).isEmpty()) {
            str_queue = bean_queue.poll();
        }
        return str_queue;
    }

    public void init() {
        if (!FacesContext.getCurrentInstance().isPostback()) {
            ProcessaJSON();
        }
    }

    public void ProcessaJSON() {
        String msg;
        if (null!=(msg=getMensagem())) {
            try {
                long id_Home;
                String home_name;
                ArrayList<SensorEquipament> sequiplist = new ArrayList();
                SensorEquipament sensor_eq = null;
                EquipamentEvent equi_ev;
                SensorHistoric sensor_hist;

                JSONObject json = (JSONObject) parser.parse(msg);
                JSONArray sensors = (JSONArray) json.get("sensors");
                JSONObject info = (JSONObject) json.get("info");
                String timestamp = (String) info.get("timeStamp");

                for (int i = 0; i < sensors.size(); i++) {
                    JSONObject attr = (JSONObject) sensors.get(i);
                    equi_ev = new EquipamentEvent((Long) attr.get("state") > 0, timestamp);
                    sensor_hist = new SensorHistoric((Long) attr.get("value"), timestamp);
                    long id_Sensor = (Long) attr.get("id_Sensor_Equipment");
                    String name_equip = (String) attr.get("name_equipment");
                    String type = (String) attr.get("type");
                    String zone = (String) attr.get("zone");
                    sensor_eq = new SensorEquipament(id_Sensor, name_equip, type, zone, sensor_hist, equi_ev);
                    sequiplist.add(sensor_eq);
                }

                id_Home = (Long) info.get("id_Home");
                home_name = (String) info.get("home_name");

                sensorsList.add(new Home(id_Home, home_name, sequiplist));

            } catch (ParseException ex) {
                Logger.getLogger(SensorsBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
