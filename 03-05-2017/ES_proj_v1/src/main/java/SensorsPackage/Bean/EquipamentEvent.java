/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage.Bean;

import java.io.Serializable;
/**
 *
 * @author aricson
 */
public class EquipamentEvent implements Serializable{
    
    private boolean state;
    private String timestamp;

    public EquipamentEvent(boolean state, String timestamp) {
        this.state = state;
        this.timestamp = timestamp;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }       
    
}
