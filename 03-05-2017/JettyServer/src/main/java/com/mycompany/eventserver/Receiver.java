/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.eventserver;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import javax.websocket.Session;

/**
 *
 * @author aricson
 */
public class Receiver extends Thread {

    private static final String EXCHANGE_NAME = "SensorData";
    private ConnectionFactory factory = null;
    private final static Logger logger = Logger.getLogger(Receiver.class);

    @Override
    public void run() {

        try {

            factory = new ConnectionFactory();
            factory.setHost("localhost");
            factory.setPort(5672);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, EXCHANGE_NAME, "");

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
            //logger.debug(" [*] Waiting for messages. To exit press CTRL+C");

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                        AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");
                    SendMessage(message);
                    System.out.println(" [x] Received '" + message + "'");
                    //logger.debug(" [x] Received '" + message + "'");

                }
            };
            channel.basicConsume(queueName, true, consumer);

            //sess.getBasicRemote().sendText("teste");
        } catch (IOException | TimeoutException ex) {
            logger.error(ex);
        }
    }

    public static void SendMessage(String m) {
        for (Session session : EventSocket.getAllSessions()) {
            try {
                System.out.println("Sending Message To: " + session.getId());
                session.getBasicRemote().sendText(m);
            } catch (IOException ex) {
                logger.error(ex);
            }
        }
    }
}
