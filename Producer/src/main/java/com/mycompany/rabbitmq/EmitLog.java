/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rabbitmq;

/**
 *
 * @author aricson
 */

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

public class EmitLog {

    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] argv) throws java.io.IOException, java.util.concurrent.TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        
        try {
            while (true) {
                //String message = getMessage(argv);
                JSONObject sdata = new SensorData().GetData();
                channel.basicPublish(EXCHANGE_NAME, "", null, sdata.toJSONString().getBytes());
                System.out.println(" [x] Sent '" + sdata + "'");
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(EmitLog.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            channel.close();
            connection.close();
        }        
    }
}