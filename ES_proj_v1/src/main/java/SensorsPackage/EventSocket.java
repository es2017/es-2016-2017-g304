/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage;

/**
 *
 * @author aricson
 */
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;

@ClientEndpoint
public class EventSocket
{ 
    //private static String mensagem;
    @OnOpen
    public void onWebSocketConnect(Session sess)
    {
        // sess.getBasicRemote().sendText("testeclient");
        System.out.println("Socket Connected: " + sess);
    }
    
    @OnClose
    public void onWebSocketClose(CloseReason reason)
    {
        System.out.println("Socket Closed: " + reason);
    }
    
    @OnError
    public void onWebSocketError(Throwable cause)
    {
        cause.printStackTrace(System.err);
    }
    
}