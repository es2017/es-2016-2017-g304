/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.eventserver;

/**
 *
 * @author aricson
 */
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value="/events/")
public class EventSocket
{ 
    private static Set<Session> allSessions = Collections.synchronizedSet(new HashSet<Session>());

    public static Set<Session> getAllSessions() {
        return allSessions;
    }

    public static void setAllSessions(Set<Session> allSessions) {
        EventSocket.allSessions = allSessions;
    }
    
    @OnOpen
    public void onWebSocketConnect(Session sess)
    {
        allSessions.add(sess);
        System.out.println("Socket Connected: " + sess);
    }
    
    @OnMessage
    public void onWebSocketText(String message)
    {
        System.out.println("Received TEXT message: " + message);
    }
    
    @OnClose
    public void onWebSocketClose(CloseReason reason,Session sess)
    {
        allSessions.remove(sess);
        System.out.println("Socket Closed: " + reason);
    }
    
    @OnError
    public void onWebSocketError(Throwable cause)
    {
        cause.printStackTrace(System.err);
    }

}