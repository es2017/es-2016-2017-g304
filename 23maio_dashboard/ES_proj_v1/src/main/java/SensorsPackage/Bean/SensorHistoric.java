/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage.Bean;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author aricson
 */
public class SensorHistoric implements Serializable{
    
    private long value;
    private String timestamp; 
    
    public SensorHistoric(long value, String timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
        
}
