/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage.Bean;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author aricson
 */
public class AlarmHistoric implements Serializable{
    
    private long value;
    private Timestamp timestamp;

    public AlarmHistoric(long value, Timestamp timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
    
}
