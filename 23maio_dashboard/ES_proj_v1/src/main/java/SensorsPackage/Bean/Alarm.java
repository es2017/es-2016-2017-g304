/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage.Bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author aricson
 */
public class Alarm implements Serializable{
    
    private long interval;
    private ArrayList<AlarmHistoric> alarm_hist;

    public Alarm(long interval, ArrayList<AlarmHistoric> alarm_hist) {
        this.interval = interval;
        this.alarm_hist = alarm_hist;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public ArrayList<AlarmHistoric> getAlarm_hist() {
        return alarm_hist;
    }

    public void setAlarm_hist(ArrayList<AlarmHistoric> alarm_hist) {
        this.alarm_hist = alarm_hist;
    }
    
    
}
