/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SensorsPackage.Bean;

import java.io.Serializable;

/**
 *
 * @author aricson
 */
public class SensorEquipament implements Serializable{
    
    private long id_sensor_equipament;
    private String name_equipament;
    private String type;
    private String zone;
    private SensorHistoric s_hist;
    private EquipamentEvent equi_event;

    public SensorEquipament(long id_sensor_equipament, String name_equipament, String type, String zone, SensorHistoric s_hist, EquipamentEvent equi_event) {
        this.id_sensor_equipament = id_sensor_equipament;
        this.name_equipament = name_equipament;
        this.type = type;
        this.zone = zone;
        this.s_hist = s_hist;
        this.equi_event = equi_event;
    }

    public long getId_sensor_equipament() {
        return id_sensor_equipament;
    }

    public void setId_sensor_equipament(long id_sensor_equipament) {
        this.id_sensor_equipament = id_sensor_equipament;
    }

    public String getName_equipament() {
        return name_equipament;
    }

    public void setName_equipament(String name_equipament) {
        this.name_equipament = name_equipament;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public SensorHistoric getS_hist() {
        return s_hist;
    }

    public void setS_hist(SensorHistoric s_hist) {
        this.s_hist = s_hist;
    }

    public EquipamentEvent getEqui_event() {
        return equi_event;
    }

    public void setEqui_event(EquipamentEvent equi_event) {
        this.equi_event = equi_event;
    }
        
}
