package SensorsPackage.Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import SensorsPackage.EventClient;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import javafx.scene.chart.LineChart;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

@ManagedBean(name = "sensors")
@SessionScoped
public class SensorsBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private EventClient singleton;

    private final ArrayList<Home> sensorsList = new ArrayList();
    private Queue<String> bean_queue = new LinkedList();

    private final JSONParser parser = new JSONParser();
    //events selected 
    private String id_house_selected = "0";
    private String id_sensor_selected = "0";

    private Map<String, ArrayList<SensorEquipament>> sen= new HashMap<>();
    private Map<String, Map<String, ArrayList<SensorEquipament>>> homes= new HashMap<>();
    //variaveis atuais 
    private Home atual;
    private long value=0;
    private int count=0;
    
    //Graph 
    private LineChartModel lineModel;
    LineChartModel model= new LineChartModel();
    LineChartSeries serie1;
    
    public LineChartModel getLineModel(){
        return lineModel;
    }

    public ArrayList<Home> getSensorsList() {
        return sensorsList;
    }

    // select house
    public String getId_house_selected() {

        return id_house_selected;
    }

    public void setId_house_selected(String id_house_selected) {

        this.id_house_selected = id_house_selected;
    }

    // select sensor
    public String getId_sensor_selected() {

        return id_sensor_selected;
    }

    public void setId_sensor_selected(String id_house_selected) {

        this.id_sensor_selected = id_sensor_selected;
    }

    public void set_Id_house(String id_house_selected) {
        //System.out.println("entrei="+id_house_selected);
        this.id_house_selected = id_house_selected;
        //System.out.println("actualizado="+id_house_selected);
    }

    //change house id selected 
    public void valueChangeMethod(ValueChangeEvent e) {
        this.id_house_selected = e.getNewValue().toString();
        lineModel = criarLinhasModelo();
        ProcessaAtual();
    }

    //change sensor id selected 
    public void valueChangeMethodSensor(ValueChangeEvent e) {
        System.out.println("id_sensor= " + id_sensor_selected);
        this.id_sensor_selected = e.getNewValue().toString();
        ProcessaAtual();
        System.out.println("id_sensor= " + id_sensor_selected);

    }

    //variavel atual home get and set 
    public Home getAtual() {
        return atual;
    }

    public void setAtual(Home atual) {
        this.atual = atual;
    }

    private String getMensagem() {

        String str_queue = null;
        if (!(bean_queue = singleton.getQueue()).isEmpty()) {
            str_queue = bean_queue.poll();
        }
        return str_queue;
    }

    public void init() {
        if (!FacesContext.getCurrentInstance().isPostback()) {
            ProcessaJSON();
            iniciarModeloLinear();
        }
    }
    //Live Graph
    public void iniciarModeloLinear(){
        
        count=0;
        
        serie1 = new LineChartSeries();
        lineModel = new LineChartModel();
        serie1.setLabel("Temperatura");
        lineModel.setTitle("Exemplo");
        lineModel.setLegendPosition("e");
        
        Axis yAxis = lineModel.getAxis(AxisType.Y);
        yAxis.setMin(-20);
        yAxis.setMax(100);
        yAxis.setTickFormat("%d");
        yAxis.setLabel("Sensor");
          
        Axis xAxis = lineModel.getAxis(AxisType.X);
        xAxis.setMin(0);
        xAxis.setMax(20);
        xAxis.setTickFormat("%d");
        xAxis.setLabel("Tempo");
        serie1.set(0,0);    
    }
    //Live Graph
    public LineChartModel criarLinhasModelo(){
        count=0;
        lineModel = new LineChartModel();
        serie1 = new LineChartSeries();
        serie1.setLabel("Temperatura");
        lineModel.addSeries(serie1);
        lineModel.setTitle("Exemplo");
        lineModel.setLegendPosition("e");
        
        Axis yAxis = lineModel.getAxis(AxisType.Y);
        yAxis.setMin(-20);
        yAxis.setMax(100);
        yAxis.setTickFormat("%d");
        yAxis.setLabel("Sensor");
        
        Axis xAxis = lineModel.getAxis(AxisType.X);
        xAxis.setMin(0);
        xAxis.setMax(20);
        xAxis.setTickFormat("%d");
        xAxis.setLabel("Tempo");
        serie1.set(0,0);
            
       return lineModel;
    
    }
    //Selected Data in homexhtml
    public void ProcessaAtual() {
        
        for (Map.Entry<String, Map<String, ArrayList<SensorEquipament>>> outer : homes.entrySet()) {
            if (outer.getKey().equals(getId_house_selected())) {
                for (Map.Entry<String, ArrayList<SensorEquipament>> inner : outer.getValue().entrySet()) {
                    if (inner.getKey().equals(id_sensor_selected)) {
                        
                        long l = Long.parseLong(outer.getKey());
                        String name= "";
                        for(int i =0; i< sensorsList.size();i++){
                            if(sensorsList.get(i).getId_home() == l){
                                name= sensorsList.get(i).getName();
                            }           
                        }
                        atual = new Home(l, name, inner.getValue());
                    }
                }
            }
        }
    }
    
    //process data
    public void ProcessaJSON() {
        String msg;
        if (null != (msg = getMensagem())) {
            try {

                long id_Home;
                String home_name;
                
                SensorEquipament sensor_eq = null;
                EquipamentEvent equi_ev;
                SensorHistoric sensor_hist;

                JSONObject json = (JSONObject) parser.parse(msg);
                JSONArray sensors = (JSONArray) json.get("sensors");
                JSONObject info = (JSONObject) json.get("info");
                String timestamp = (String) info.get("timeStamp");

                home_name = (String) info.get("home_name");
                id_Home = (Long) info.get("id_Home");
                String id_home = Long.toString(id_Home);

                for (int i = 0; i < sensors.size(); i++) {
                    ArrayList<SensorEquipament> sequiplist = new ArrayList();
                    JSONObject attr = (JSONObject) sensors.get(i);
                    equi_ev = new EquipamentEvent((Long) attr.get("state") > 0, timestamp);
                    sensor_hist = new SensorHistoric((Long) attr.get("value"), timestamp);
                    long id_Sensor = (Long) attr.get("id_Sensor_Equipment");
                    String name_equip = (String) attr.get("name_equipment");
                    String type = (String) attr.get("type");
                    String zone = (String) attr.get("zone");
                    String strid_sensor = Long.toString(id_Sensor);
            
                    sensor_eq = new SensorEquipament(id_Sensor, name_equip, type, zone, sensor_hist, equi_ev);  
                    sequiplist.add(sensor_eq);
                    
                    //last house id and sensor id sended to map  (Selected Data)
                    sen.put(strid_sensor, sequiplist);
                    homes.put(id_home, sen);
                      
                    //add home to Live Houses Data
                    sensorsList.add(new Home(id_Home, home_name, sequiplist));
                    
                    //temperature of house id and sensor id selected go to Live Graph
                    if(getId_house_selected().equals(id_home)  && type.equals("Temperatura")){
                        serie1.set(count++,sensor_hist.getValue());
                    } 
                }
                
            } catch (ParseException ex) {
                Logger.getLogger(SensorsBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

