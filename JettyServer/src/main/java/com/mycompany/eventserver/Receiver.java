/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.eventserver;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.Session;

/**
 *
 * @author aricson
 */
public class Receiver extends Thread{
    
    private static final String EXCHANGE_NAME = "logs";
        
    @Override
    public void run() {

        try {

            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, EXCHANGE_NAME, "");

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                        AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");
                    SendMessage(message);
                    System.out.println(" [x] Received '" + message + "'");
                }
            };
            channel.basicConsume(queueName, true, consumer);

            //sess.getBasicRemote().sendText("teste");
        } catch (IOException | TimeoutException ex) {
            Logger.getLogger(EventSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        public static void SendMessage(String m){
        for (Session session : EventSocket.getAllSessions()) {
            try {
                System.out.println("Sending Message To: " + session.getId());
                session.getBasicRemote().sendText(m);
            } catch (IOException ex) {
                Logger.getLogger(EventSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
   }
    }
}
